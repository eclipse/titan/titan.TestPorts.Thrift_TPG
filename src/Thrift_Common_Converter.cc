///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
//////////////////////////////////////////////////////////////////////////////


#include "Thrift_Common.hh"
#include "Thrift_Common_Converter.hh"

void conv_ttcn_thrift(const Thrift__Common::bool_& in,bool& out){
  out=in;
}
void conv_ttcn_thrift(const Thrift__Common::byte& in,int8_t& out){
  out=(int)in;
}
void conv_ttcn_thrift(const Thrift__Common::i16& in,int16_t& out){
  out=(int)in;
}
void conv_ttcn_thrift(const Thrift__Common::i32& in,int32_t& out){
  out=(int)in;
}
void conv_ttcn_thrift(const Thrift__Common::i64& in,int64_t& out){
  out=in.get_long_long_val();
}
void conv_ttcn_thrift(const Thrift__Common::string& in,std::string& out){
  out=std::string((const char*)in,in.lengthof());
}
void conv_ttcn_thrift(const Thrift__Common::binary& in,std::string& out){
  out=std::string((const char*)(const unsigned char*)in,in.lengthof());
}

void conv_thrift_ttcn(const bool& in,Thrift__Common::bool_& out){
  out=in;
}

void conv_thrift_ttcn(const int8_t& in,Thrift__Common::byte& out){
  out=in;
}

void conv_thrift_ttcn(const int16_t& in,Thrift__Common::i16& out){
  out=in;
}

void conv_thrift_ttcn(const int32_t& in,Thrift__Common::i32& out){
  out=in;
}

void conv_thrift_ttcn(const int64_t& in,Thrift__Common::i64& out){
  out.set_long_long_val(in);
}
void conv_thrift_ttcn(const std::string& in,Thrift__Common::string& out){
  out=Thrift__Common::string(in.size(),in.data());
}
void conv_thrift_ttcn(const std::string& in,Thrift__Common::binary& out){
  out=Thrift__Common::binary(in.size(),(const unsigned char*)in.data());
}
