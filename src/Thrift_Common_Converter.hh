///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
//////////////////////////////////////////////////////////////////////////////


#ifndef Thrift_Common_Converter_H
#define Thrift_Common_Converter_H

#include <thrift/Thrift.h>
#include "Thrift_Common.hh"


struct internal_comm_struct{
  int code;
  void** data;
};


void conv_ttcn_thrift(const Thrift__Common::bool_& in,bool& out);
void conv_ttcn_thrift(const Thrift__Common::byte& in,int8_t& out);
void conv_ttcn_thrift(const Thrift__Common::i16& in,int16_t& out);
void conv_ttcn_thrift(const Thrift__Common::i32& in,int32_t& out);
void conv_ttcn_thrift(const Thrift__Common::i64& in,int64_t& out);
void conv_ttcn_thrift(const Thrift__Common::string& in,std::string& out);
void conv_ttcn_thrift(const Thrift__Common::binary& in,std::string& out);
void conv_thrift_ttcn(const bool& in,Thrift__Common::bool_& out);
void conv_thrift_ttcn(const int8_t& in,Thrift__Common::byte& out);
void conv_thrift_ttcn(const int16_t& in,Thrift__Common::i16& out);
void conv_thrift_ttcn(const int32_t& in,Thrift__Common::i32& out);
void conv_thrift_ttcn(const int64_t& in,Thrift__Common::i64& out);
void conv_thrift_ttcn(const std::string& in,Thrift__Common::string& out);
void conv_thrift_ttcn(const std::string& in,Thrift__Common::binary& out);

#endif
