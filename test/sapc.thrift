#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Data Structures
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

typedef i32                ScfId
typedef i32                TenantId
typedef i32                ServChainId
typedef i32                PortId
typedef i32                NexthopId
typedef i32                Priority
typedef i32                SubFilterId
typedef i32                ApplFilterId
typedef i16                SubProfileId
typedef i16                ApplProfileId
typedef i16                VlanId

typedef byte         IpProtocol

typedef string      TenantName

typedef i64                MacAddress

enum Afi {
     AFI_IPV4,
     AFI_IPV6,
}

struct IpAddress {
     1: required     Afi                  afi,
     2: optional          i32                  ipv4Address,
     3: optional          binary               ipv6Address,
}

struct IpSubnet {
     1: required     Afi                  afi,
     2: optional          i32                  ipv4Prefix,
     3: optional          binary               ipv6Prefix,
     4: required          byte            prefixLength,
}

enum DpnState {
     DPN_STATE_UNKNOWN =           0, 
     DPN_STATE_UNCONFIGURED =      1,
     DPN_STATE_PRECONFIGURED =     2,
     DPN_STATE_CONNECTED =         3,
     DPN_STATE_READY =             4,
     DPN_STATE_DISCONNECTED =      5,
}

enum PortState {
     PORT_STATE_UNKNOWN =          0,
     PORT_STATE_UNCONFIGURED =     1,
     PORT_STATE_PRECONFIGURED =    2,
     PORT_STATE_UP =               3,
     PORT_STATE_DOWN =             4,
     PORT_STATE_SHUT =             5,
}

enum LogicalPortUse {
     PORT_USE_UNKNOWN =            0,
     PORT_USE_FREE =                    1,
     PORT_USE_SEIZED =             2,
     PORT_USE_BUSY =                    3,
}

struct LogicalPortInfo {
     1: required     LogicalPortUse  portUse,
     2: optional     VlanId               vlanId,
     3: optional     string               displayName,
}

struct PortInfo {
     1: optional          i64                  dpId,
     2: required          string               portName,
     3: required          PortState       portState,
     4: optional          i32                  portNo,
     5: optional          list<LogicalPortInfo>     logicalPorts,
}

struct DpnInfo {
     1: required          i64                  dpId,       
     2: optional          string               dpnName,
     3: optional          list<PortInfo> ports,
     4: required          DpnState        dpnState,
}

struct DomainInfo {
     1: optional          string               domainName,
     2: required          list<DpnInfo>  dpns,
}


struct TenantInfo {
     1: required     TenantName           tenantName,
     2: optional     IpAddress            tenantIpAddress,
     3: optional         i16                         tcpPortNo,
     4: optional          i16                        cookie,
} 

enum SubEventType {
     CREATE = 0,
     UPDATE = 1,
     DELETE = 2,
}

enum SubObjectType {
     DPN = 0,
     PORT = 1,
}

struct SubItem {
     1: required     SubEventType               subEventType,
     2: required     SubObjectType              subObjectType,
}

struct LogicalPort {
     1: required i64                 dpId,
     2: required string              portName,
     3: optional VlanId              vlanId,
     4: optional string              displayName,
}

struct ServiceChain {
     1: optional string              displayName,
}


enum Direction {
     UP = 1,
     DOWN = 2,
 
}


struct SubFilterInfo {
     1: optional string                   displayName,
     2: optional IpSubnet            srcIpPrefix,
     3: optional Priority           priority, 
     4: optional SubProfileId       subProfileId,
}

struct ApplFilterInfo {
     1: optional string                   displayName
     2: optional IpSubnet            dstIpPrefix,
     3: optional IpProtocol              ipProtocol,
     4: optional i32                     destPortNo,
     5: optional Priority           priority,
     6: optional ApplProfileId      applProfileId,
}


typedef list<NexthopId>              NextHops

struct IndirectNexthop {
     1: optional string              displayName,
     2: optional NextHops           nextHops,
}

struct DirectNexthop {
     1: optional string                   displayName,
     2: optional PortId                   egressPort,
     3: optional IpAddress           ipAddress,
     4: optional MacAddress               macAddress,
}

enum Result{
     UNSUCCESSFUL = 0,
     SUCCESSFUL = 1,
}

struct ScfResult {
     1: required          Result               result,
     2: optional          TenantId        tenantId,     /* Response to register_tenant() */                     
     3: optional          i16                  genId,             /* Response to register_tenant(), create_scf_instance(), seize_logical_port(), add_logical_port, create_service_chain(), start_recovery(), recovery_completed() */
     4: optional          i32                  revisionId,        /* Response to register_tenant(), create_scf_instance(), seize_logical_port(), add_logical_port, create_service_chain(), start_recovery(), recovery_completed() */
     5: optional          PortInfo        portInfo,     /* Response to seize_port(), get_port() */
     6: optional          DpnInfo              dpnInfo,      /* Response to get_dpn() */
}
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Service Definition
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

service TenantService {

     # Tenant handling
 
     ScfResult  register_tenant                   (1:  TenantInfo         tenantInfo),  
 
     ScfResult  deregister_tenant                 (1:  TenantId           tenantId),  
 
     ScfResult  start_recovery                    (1:  TenantInfo         tenantInfo),
 
     ScfResult  recovery_completed                (1:  TenantInfo         tenantInfo),
                 
     ScfResult  subscribe_nw_events               (1:  TenantId           tenantId,
                                                              2:  SubItem            subItem),
                                                            
     ScfResult  unsubscribe_nw_events        (1:  TenantId            tenantId,
                                                           2:  SubItem              subItem),             
 
     # Logical Port handling
                                                      
     ScfResult  seize_logical_port               (1:  TenantId           tenantId,
                                                              2:  PortId             portId,
                                                              3:  LogicalPort        logicalPort),
                                                              
     ScfResult  update_logical_port              (1:  TenantId           tenantId,
                                                              2:  PortId             portId,
                                                              3:  LogicalPort        logicalPort),
                                                              
     ScfResult  release_logical_port         (1:  TenantId            tenantId,
                                                              2:  PortId             portId),
}


service NetworkInfoService {

     # Domain information retrieval
 
     DomainInfo      get_domain                        (),
 
     ScfResult get_dpn                                (1:  i64                      dpId), 
 
     ScfResult  get_port                          (1:  i64                      dpId, 
                                                              2:  string             portName),
 
}


service ServiceChainProvisioning {

     #SCF Instance handling
 
     ScfResult  create_scf_instance            (1:  TenantId              tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  string             displayName), 
                                                              
     ScfResult  delete_scf_instance            (1:  TenantId              tenantId,
                                                              2:  ScfId              scfId),
                                                              
     ScfResult  add_logical_port                  (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  PortId             portId,
                                                              4:  Direction          direction) 

     ScfResult  remove_logical_port               (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  PortId             portId)                                                               
                                                 
 

     # Service Chain handling
     ScfResult  create_service_chain         (1:  TenantId            tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  ServChainId        servChainId,
                                                              4:  ServiceChain       serviceChain),
                                                             
     ScfResult  update_service_chain         (1:  TenantId            tenantId,     
                                                              2:  ScfId              scfId,
                                                              3:  ServChainId        servChainId,
                                                              4:  ServiceChain       serviceChain),
                                                              
     ScfResult  delete_service_chain         (1:  TenantId            tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  ServChainId        servChainId),
                                                 
 
     # SCF nexthop handling

     ScfResult  create_repl_nexthop               (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  NexthopId          nexthopId
                                                              4:  IndirectNexthop     replNexthop),
                                                              
     ScfResult  update_repl_nexthop               (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  NexthopId          nexthopId
                                                              4:  IndirectNexthop     replNexthop),
                                                              
     ScfResult  create_lb_nexthop                 (1:  TenantId           tenantId,   
                                                              2:  ScfId              scfId,
                                                              3:  NexthopId          nexthopId,
                                                              4:  IndirectNexthop  lbNexthop),
                                                             
     ScfResult  update_lb_nexthop                 (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  NexthopId          nexthopId,
                                                              4:  IndirectNexthop  lbNexthops),
                                                              
     ScfResult  create_direct_nexthop        (1:  TenantId            tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  NexthopId          nexthopId,
                                                              4:  DirectNexthop     directNexthop),
                                                              
     ScfResult  update_direct_nexthop        (1:  TenantId            tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  NexthopId          nexthopId,
                                                              4:  DirectNexthop     directNexthop),
                                                              
     ScfResult  delete_nexthop                    (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  NexthopId          nexthopId),
 
     # SC Hop handling
     ScfResult  create_sc_hop                     (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  ServChainId        servChainId,
                                                              4:  PortId             ingressPort,
                                                              5:  NexthopId          nexthopId),
                                                              
     ScfResult  update_sc_hop                     (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  ServChainId        servChainId,
                                                              4:  PortId             ingressPort,
                                                              5:  NexthopId          nexthopId),
                                                              
     ScfResult  delete_sc_hop                     (1:  TenantId           tenantId,
                                                              2:  ScfId              scfId,
                                                              3:  ServChainId        servChainId,
                                                              4:  PortId             ingressPort), 
}


service FlowMapping {
     # Profile handling
     ScfResult  create_sub_profile              (1:  TenantId              tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  SubProfileId        subProfileId,
                                                             4:  string              displayName),
                                                            
     ScfResult  delete_sub_profile              (1:  TenantId            tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  SubProfileId        subProfileId),
                                                            
     ScfResult  create_app_profile              (1:  TenantId            tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  ApplProfileId       applProfileId,
                                                             4:  string              displayName),
                                                            
     ScfResult  delete_app_profile              (1:  TenantId            tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  ApplProfileId       applProfileId),
 
   # SCF Service Chain mapping
 
     ScfResult  create_profile_map              (1:  TenantId            tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  SubProfileId       subProfileId,  /* Change to struct? */
                                                             4:  ApplProfileId      applProfileId,
                                                             5:  ServChainId         servChainId),
                                                 
     ScfResult  update_profile_map              (1:  TenantId            tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  SubProfileId       subProfileId,
                                                             4:  ApplProfileId      applProfileId,
                                                             5:  ServChainId         servChainId),
                                                 
     ScfResult  delete_profile_map          (1:  TenantId             tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  SubProfileId       subProfileId,
                                                             4:  ApplProfileId      applProfileId),  

     # Filters
     ScfResult  create_exc_filter                (1:  TenantId            tenantId,
                                                             2:  ScfId                   scfId,
                                                             3:  IpAddress           srcIp,
                                                             4:  IpAddress          destIp,
                                                             5:  IpProtocol          ipProtocol,
                                                             6:  PortId              srcPort,
                                                             7:  PortId              destPort,
                                                             8:  ServChainId        servChainId),
                                                            
     ScfResult  delete_exc_filter           (1:  TenantId             tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  IpAddress          srcIp,
                                                             4:  IpAddress          destIp,
                                                             5:  IpProtocol          ipProtocol,
                                                             6:  PortId              srcPort,
                                                             7:  PortId              destPort),
                                                            
     ScfResult  create_sub_filter           (1:  TenantId             tenantId   
                                                             2:  ScfId                     scfId,
                                                             3:  SubFilterId        subFilterId,
                                                             4:  SubFilterInfo       subFilterInfo),
                                                            
     ScfResult  update_sub_filter           (1:  TenantId             tenantId
                                                             2:  ScfId                     scfId,
                                                             3:  SubFilterId        subFilterId,
                                                             4:  SubFilterInfo       subFilterInfo),
                                                            
     ScfResult  delete_sub_filter               (1:  TenantId            tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  SubFilterId        subFilterId ),
                                                            
    ScfResult   create_app_filter           (1:  TenantId             tenantId,   
                                                             2:  ScfId                     scfId,
                                                             3:  ApplFilterId       applFilterId,
                                                             4:  ApplFilterInfo     applFilterInfo ),
                                                            
     ScfResult  update_app_filter           (1:  TenantId             tenantId,   
                                                             2:  ScfId                     scfId,
                                                             3:  ApplFilterId       applFilterId,
                                                             4:  ApplFilterInfo     applFilterInfo ),
                                                            
     ScfResult delete_app_filter                (1:  TenantId            tenantId,
                                                             2:  ScfId                     scfId,
                                                             3:  ApplFilterId       applFilterId ),                                               
                                                 
}
